class Tperfiles < ApplicationRecord
  has_many :tbitacora, foreign_key: :IdPerfil
  belongs_to :tgrupos, foreign_key: :IdGrupo
end
