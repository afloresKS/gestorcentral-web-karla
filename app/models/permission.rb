class Permission < ApplicationRecord
  belongs_to :profile, foreign_key: :profile_id
  belongs_to :view, foreign_key: :view_id
end
