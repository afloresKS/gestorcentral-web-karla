class TxSessionsController < ApplicationController
  before_action :set_tx_session, only: [:show, :edit, :update, :destroy]

  # GET /tx_sessions
  # GET /tx_sessions.json
  def index
    @tx_sessions = TxSessions.all


  end

  # GET /tx_sessions/1
  # GET /tx_sessions/1.json
  def show
  end

  # GET /tx_sessions/new
  def new
    @tx_session = TxSessions.new
    @edt = false;

    tFormatos = Tformatos.all
    gon.formatos = tFormatos

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays
    gon.false = true



  end

  # GET /tx_sessions/1/edit
  def edit
  end

  # POST /tx_sessions
  # POST /tx_sessions.json
  def create
    @tx_session = TxSessions.new(tx_session_params)

    respond_to do |format|
      if @tx_session.save


        @session_id = @tx_session.id
        @tx_session_filter = TxFilter.new

        format.html { redirect_to @tx_session, notice: 'Tx session was successfully created.' }
        format.json { render :show, status: :created, location: @tx_session }
      else
        format.html { render :new }
        format.json { render json: @tx_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tx_sessions/1
  # PATCH/PUT /tx_sessions/1.json
  def update
    respond_to do |format|
      if @tx_session.update(tx_session_params)
        format.html { redirect_to @tx_session, notice: 'Tx session was successfully updated.' }
        format.json { render :show, status: :ok, location: @tx_session }
      else
        format.html { render :edit }
        format.json { render json: @tx_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tx_sessions/1
  # DELETE /tx_sessions/1.json
  def destroy
    @tx_session.destroy
    respond_to do |format|
      format.html { redirect_to tx_sessions_url, notice: 'Tx session was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_session
      @tx_session = TxSessions.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_session_params
      params.require(:tx_sessions).permit(:Lay_id, :Format_id, :Description, :Sessioncolor, :Interval, :Inactive)
    end
end
