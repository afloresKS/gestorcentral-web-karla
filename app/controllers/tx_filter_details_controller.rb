class TxFilterDetailsController < ApplicationController
  before_action :set_tx_filter_detail, only: [:show, :edit, :update, :destroy]

  # GET /tx_filter_details
  # GET /tx_filter_details.json
  def index
    @tx_filter_details = TxFilterDetail.all
  end

  # GET /tx_filter_details/1
  # GET /tx_filter_details/1.json
  def show
  end

  # GET /tx_filter_details/new
  def new
    @tx_filter_detail = TxFilterDetail.new
  end

  # GET /tx_filter_details/1/edit
  def edit
  end

  # POST /tx_filter_details
  # POST /tx_filter_details.json
  def create


    # if( params[:_method].present? && params[:_method] == 'create' )
    #   @tx_filter_detail = TxFilterDetail.new
    #   @tx_filter_detail.connector = params[:connector];
    #   @tx_filter_detail.lenght = params[:lenght];
    #   @tx_filter_detail.denied = params[:denied];
    #   @tx_filter_detail.operator = params[:operator];
    #   @tx_filter_detail.parenthesis = params[:parenthesis];
    #   @tx_filter_detail.position = params[:position];
    #   @tx_filter_detail.value = params[:value];
    #   @tx_filter_detail.position = params[:position];
    #
    # else
    #
    # end
    #
    #
    @tx_filter_detail = TxFilterDetail.new(tx_filter_detail_params)

    @true = true;

    @tx_filter_detail.save

    # respond_to do |format|
    #   if @tx_filter_detail.save
    #     format.html { redirect_to @tx_filter_detail, notice: 'Tx filter detail was successfully created.' }
    #     format.json { render :show, status: :created, location: @tx_filter_detail }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @tx_filter_detail.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /tx_filter_details/1
  # PATCH/PUT /tx_filter_details/1.json
  def update
    respond_to do |format|
      if @tx_filter_detail.update(tx_filter_detail_params)
        format.html { redirect_to @tx_filter_detail, notice: 'Tx filter detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @tx_filter_detail }
      else
        format.html { render :edit }
        format.json { render json: @tx_filter_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tx_filter_details/1
  # DELETE /tx_filter_details/1.json
  def destroy
    @tx_filter_detail.destroy
    respond_to do |format|
      format.html { redirect_to tx_filter_details_url, notice: 'Tx filter detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_filter_detail
      @tx_filter_detail = TxFilterDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_filter_detail_params
      params.require(:tx_filter_detail).permit(:filter_id, :conditionnumber, :connector, :denied, :operator, :parenthesis, :value, :fieldFormat_id, :position, :lenght)
    end
end
