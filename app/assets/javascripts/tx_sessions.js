//= require toastr/toastr.min.js
//= require nestable/jquery.nestable.js
//= require codemirror/codemirror.js
//= require codemirror/mode/javascript/javascript.js
//= require validate/jquery.validate.min.js
//= require jsTree/jstree.min.js
//= require diff_match_patch/javascript/diff_match_patch.js
//= require preetyTextDiff/jquery.pretty-text-diff.min.js
//= require tinycon/tinycon.min.js
//= require idle-timer/idle-timer.min.js
//= require jquery-ui/jquery-ui.min.js
//= require sweetalert/sweetalert.min.js

console.log('EntreATXSessions');

//---Estilos para el wizard
$('.content').css('min-height','auto');
$('.content').css('height','auto');

function recargar(){
    window.location.reload(true);
};

$('.eliminarSesion').on('click', function(){
    alert("Entree Eliminar");

    let id =  this.id;

    swal({
        title: "Your data will lose.",
        text: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, continue anyway!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                },
                url: "/tx_sessions/" + id,
                data: {"_method": "delete"},
            });
            swal("Your hisotic session data might lose...", "success");
            recargar();
        }
        else {
            swal("Cancelled", "Your data is safe.", "error");
        }
    });
});


//############################## SCRIPTS PARA EL METODO NEW ############//
//----Gon Para llenar Select Formatos
$('.selectLay').on('change', function(){
    $(".selectFormat option").remove();
    $(".selectFormat").attr('disabled', false);
    id_lay = $(this).val();


    $(".selectFormat").append($("<option value></option>").text("Select a Format"));
    for( f = 0; f < gon.formatos.length; f++ ){
        if( gon.formatos[f].IdLay == id_lay ){

            $(".selectFormat").append(`<option value="${gon.formatos[f].IdFormato}" name="optionFormat">${gon.formatos[f].Descripcion}</option>`)
        } else {
            $(".selectFormat option").remove();
            $(".selectFormat").attr('disabled', true);
            $(".selectFormat").append($("<option value></option>").text("Select a Format"));
        }
    }
});

//----Gon Para Llenar SelectFeild y Select Group
$('.selectFormat').on('change', function() {

    $(".selectFeild option").remove();
    $("#group_sel option").remove();
    id_format = $(this).val();
    // console.log(`valor de Layout: ${id_lay} y de Formato: ${id_format}`)

    $(".selectFeild").append($("<option value></option>").text("Select Feild"));
    for ( c = 0; c < gon.tCamposFormatos.length; c++ ){

        if( ( gon.tCamposFormatos[c].IdLay == id_lay ) && ( gon.tCamposFormatos[c].IdFormato == id_format ) ){

            for( d = 0; d < gon.tCamposLays.length; d++ ){

                if ( ( gon.tCamposLays[d].IdLay == gon.tCamposFormatos[c].IdLay ) && ( gon.tCamposLays[d].IdCampo == gon.tCamposFormatos[c].IdCampo )  ){

                    if ( gon.tCamposLays[d].Alias != ""){

                        $(".selectFeild").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Alias}</option>`);
                        $("#group_sel").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Alias}</option>`);

                    } else{
                        $(".selectFeild").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Nombre}</option>`);
                        $("#group_sel").append(`<option value="${gon.tCamposLays[d].IdCampo}" name="select_feild">${gon.tCamposLays[d].Nombre}</option>`)
                    }

                }
            }

        }
    }

});

//----Ayuda a mostrar el select-chose en el wizard
setTimeout(function(){

    $('#group_sel_chosen').css('width','100%');
    var hijo = $('.search-field').children();

    hijo.css('width', '100%')

}, 2000);

//--Muestra input Main - General - Group
$('.filterSession').on('change', function(){
    var typeFilter = $(this).val();
    // alert("Entre changue");
    // console.log(option)

    if ( typeFilter == "General" ){

        $('.colorPicker').css('display', 'block');
        $('.choseSelect').css('display', 'none');
        $('.content').css('min-height','320px');
        $('.chosen-choices').html();

        //Limpar valores de los demas inputs

        $('.chosen-select').val([]).trigger('chosen:updated');

    } else if( typeFilter == "Group" ) {

        $('.colorPicker').css('display', 'none');
        $('.choseSelect').css('display', 'block');
        $('.content').css('min-height','420px');

        //Limpar valores de los demas inputs
        $('.demo').val("");

    } else {

        $('.choseSelect').css('display', 'none');
        $('.colorPicker').css('display', 'none');
        $('.content').css('min-height','auto');

        //Limpar valores de los demas inputs
        $('.demo').val("");
        $('.chosen-select').val([]).trigger('chosen:updated');

    }
});

//----inicilizo chose-select
$(".chosen-select").chosen();


//---Crea la paleta de colores con jquery-minicolors-rails
$('.demo').each(function () {
    $(this).minicolors({
        control: $(this).attr('data-control') || 'hue',
        defaultValue: $(this).attr('data-defaultValue') || '',
        format: $(this).attr('data-format') || 'hex',
        keywords: $(this).attr('data-keywords') || '',
        inline: $(this).attr('data-inline') === 'true',
        letterCase: $(this).attr('data-letterCase') || 'lowercase',
        opacity: $(this).attr('data-opacity'),
        position: $(this).attr('data-position') || 'bottom left',
        swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
        change: function (value, opacity) {
            if (!value) return;
            if (opacity) value += ', ' + opacity;
            if (typeof console === 'object') {
                //console.log(value);
            }
        },
        theme: 'bootstrap'
    });
});

//----Detectar el click en el Boton Next del Wizard para agrandar el espacio del contenido
//----del 3. Filter Detail
// var hijo = $('div.actions').children();
// var btn_previous = hijo.children().eq(0);
// var btn_next = hijo.children().eq(1);
// //----Botones NEXT - PREVIOUS
// $(btn_next, btn_previous).on('click',function(){
//
//     var current = $('.filterDetail').hasClass('current')
//
//     if ( current == true ){
//      $('.content').css('min-height','1100px')
//     } else {
//         $('.content').css('min-height','320px')
//     }
// });
//
// //----Fieldsets
// $('#form-t-0, #form-t-1, #form-t-2').on('click', function(){
//     var current = $('.filterDetail').hasClass('current')
//
//     if ( current == true ){
//         $('.content').css('min-height','1100px')
//     } else {
//         $('.content').css('min-height','320px')
//     }
// });

//---Validaciones para mostar (VALUE) || (BETWEEN / NOT BETWEEN) || (IN / NOT)
$('.operator_sel').change(function(){

    $(".betweenValue label").remove();
    $(".inNot label").remove();
    let operadorText = $('.operator_sel option:selected').text();

    //---Validaciones para mostar (VALUE) || (BETWEEN / NOT BETWEEN) || (IN / NOT)
    if ( operadorText === " Between " || operadorText === " Not Between " ){
        $('.betweenValue').prepend(`<label style="display: block">${operadorText}</label>`);

        $('.oneValue, .inValue, .contDenied').css('display','none');
        $('.betweenValue').css('display','block');
        $('#valor, #selectList').val("");

        //---Reset CheckBox Denied
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else if( operadorText === " In " || operadorText === " Not In " ){
        $('.inNot').prepend(`<label style="display: block">${operadorText}</label>`);

        $('.oneValue, .betweenValue, .contDenied').css('display','none');
        $('.inValue').css('display','flex');
        $('#valor, .valOneBetween, .valTwoBetween').val("");

        //---Reset CheckBox Denied
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    } else {

        $('.contDenied, .oneValue').css('display', 'block');
        $('.betweenValue, .inValue').css('display','none');
        $('.valOneBetween, .valTwoBetween, #selectList').val("");

    }

});


//---Script para el boton de carga archivos falso
var btnReal = document.getElementById('realBtnFile');

$('.falseBtnFile').on('click', function(){
    btnReal.click();
});

$('.realBtnFile').change(function(){
    var valFile = $(this).val()

    if ( valFile ){
        $('.customText').html(valFile.match( /[\/\\]([\w\d\s\.\-\(\)]+)$/)[1]);
    } else {
        $('.customText').html('No se eligió archivo')
    }
});


//---Radio Button i-checks
$('.i-checks').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
});

//---Cambia el valor del input Denied
$('.icheckbox_square-green').on('ifClicked', function(){
    let _this = $('.icheckbox_square-green').hasClass('checked');

    if ( !_this ){

        $('#denied').val('1');

    } else {

        $('#denied').val('0');

    }
});

// //----DRAG & DROP Contenedor principal
// window.addEventListener('load', init);
// function init() {
//     idClass = 0;
//
//     container = document.querySelector('.container');
//
//     container.addEventListener('dragover', dragSobreContainer, false);
//     container.addEventListener('dragleave', dragSalioContainer, false);
//     container.addEventListener('drop', manejarDrop, false);
//
//     var conditions = document.getElementsByClassName('condition');
//
//     //----El setInterval pregunta cada 2 segundos por las condiones para poder agregarles los respectivos metodos y funciones
//     setInterval(function(){
//         for ( i in conditions ){
//             var condition = conditions[i]
//
//             if ( typeof condition.style != "undefined"){
//                 //---Inicia el evento de arraste Drag
//                 condition.addEventListener('dragstart', dragIniciado, false);
//
//                 //---Finaliza el evento de arraste Drag
//                 condition.addEventListener('dragend', dragFinalizado, false);
//             }
//         }
//     }, 2000);
//
//     function dragIniciado(e){
//         //---Agrego color rojo cuando estoy arrastrando el elemento
//         this.style.backgroundColor = 'tomato';
//
//         // var father = document.createElement('div');
//         // console.log(father);
//         var clone = this.cloneNode(true);
//         // console.log(clone);
//
//         // father.appendChild(clone);
//         enviados = e.dataTransfer.setData('text/html', clone.innerHTML);
//         console.log(enviados)
//     }
//
//     function manejarDrop(e){
//         e.preventDefault();
//
//         idClass++;
//         // console.log(idClass)
//
//         var datos = e.dataTransfer.getData('text/html', enviados);
//         // console.log(datos);
//         this.innerHTML += "<div class='cloneCondition btn-primary btn-xs' style='display: flex; padding: .5em; justify-content: space-between;'>" + datos + " </div>";
//
//         this.classList.remove('over');
//
//         var subContainer = $('div').hasClass('sub-SubCont');
//
//         if ( subContainer ){
//
//             $('div.sub-SubCont').remove();
//         }
//     }
//
//     function dragSobreContainer(e){
//         e.preventDefault();
//         this.classList.add('over');
//         return false;
//     }
//
//     function dragSalioContainer(e){
//         this.classList.remove('over');
//     }
//
//     function dragFinalizado(e){
//         //---Elimino color rojo cuando suelto el elemento
//         this.style.backgroundColor = '';
//     }
//
// }
//
//
// //----DRAG & DROP Sub Contenedor....
// window.addEventListener('load', initialize);
// function initialize() {
//     idClass = 0;
//
//     setInterval(function(){
//         subContainer = document.querySelector('.contSub-Sub');
//
//         subContainer.addEventListener('dragover', dragSobreSubContainer, false);
//         subContainer.addEventListener('dragleave', dragSalioSubContainer, false);
//         subContainer.addEventListener('drop', manejarSubDrop, false);
//
//         var clonesConditions = document.getElementsByClassName('cloneCondition');
//
//         //----El setInterval pregunta cada 2 segundos por las condiones para poder agregarles los respetivos metodos y funciones
//
//         for ( e in clonesConditions ){
//             var cloneCo = clonesConditions[e]
//
//             if ( typeof cloneCo.style != "undefined"){
//                 //---Inicia el evento de arraste Drag
//                 cloneCo.addEventListener('dragstart', dragComenzo, false);
//
//                 //---Finaliza el evento de arraste Drag
//                 cloneCo.addEventListener('dragend', dragTermino, false);
//             }
//         }
//     }, 2000);
//
//     function dragComenzo(e){
//         //---Agrego color rojo cuando estoy arrastrando el elemento
//         this.style.backgroundColor = 'tomato';
//
//         // var father = document.createElement('div');
//         // console.log(father);
//         var clone = this.cloneNode(true);
//         // console.log(clone);
//
//         // father.appendChild(clone);
//         enviados = e.dataTransfer.setData('text/html', clone.innerHTML);
//         console.log(enviados)
//     }
//
//     function manejarSubDrop(e){
//         e.stopPropagation();
//         e.preventDefault();
//
//         idClass++;
//         // console.log(idClass)
//
//         var datos = e.dataTransfer.getData('text/html', enviados);
//         // console.log(datos);
//         this.innerHTML += "<div class='cloneCondition btn-primary btn-xs' style='display: flex; padding: .5em; justify-content: space-between;'>" + datos + " </div>";
//
//         this.classList.remove('over');
//
//         $('.contSub-Sub').children('strong').remove();
//
//         $('.contSub-Sub').append('<div class="sub-SubCont" style="display: flex; background-color: #d6d6d6; width: 100%; height: 35px"> <strong> Arrastra aquí otra condicián </strong></div>');
//
//     }
//
//     function dragSobreSubContainer(e){
//         e.stopPropagation();
//         e.preventDefault();
//         this.classList.add('over');
//         return false;
//     }
//
//     function dragSalioSubContainer(e){
//         e.stopPropagation();
//         this.classList.remove('over');
//     }
//
//     function dragTermino(e){
//         //---Elimino color rojo cuando suelto el elemento
//         e.stopPropagation();
//         this.classList.remove('over');
//     }
//
// }

// $( function() {
//
//     $( "#tableUno" ).sortable({
//         axis: 'y',
//         cancel: '.thead-no-move'
//     });
//
//     $( "#tableUno" ).disableSelection();
//
// } );


//--DRAG & DROP SEGUNDA VERSION

// $(document).ready(function(){
//
//     $('.condition').draggable({helper: 'clone'});
//
//     $('.container').droppable({
//
//         accept: '.condition',
//
//         drop: function (ev, ui){
//             // alert("Entre kin");
//
//             var droppedCondition = $(ui.draggable).clone();
//             $(this).append(droppedCondition.addClass('btn-xs btn-primary').removeClass('condition'));
//
//         }
//
//     });
// });

// var containers = $('#drag-elements').toArray();
// containers.concat($('.drop-target').toArray());

// let condiciones = $('#drag-elements');
//
// //---Drag & Drop Dragula
// let drake = dragula([condiciones[0]],{
//
//     // isContainer: function (el){
//     //     return el.classList.contains('drop-target');
//     // },
//     //
//     // copy: function (el, source){
//     //     return source === document.querySelector('#drag-elements')
//     // },
//     //
//     // accepts: function (el, target){
//     //     return target !== document.querySelector('#drag-elements')
//     // }
//
// });

let left = $('#drag-elements');

// var drake =  dragula(containers,{
var drake =  dragula([left[0]],{
    // revertOnSpill: true
});

//Handle Events
drake.on('drag', function(el, source){

    let indexHijo = $(el).index();
    // console.log(indexHijo)

    if( indexHijo === 0){
        drake.cancel(true);
        swal({
            title: "Sorry!",
            text: "No puedes mover la primer condición",
            type: "error"
        });
    }

    // let elemento = $('#drag-elements').children();
    // let tr1 = elemento.eq(0);
    // let clase = tr1.attr('class');
    // let clase1 = clase.substr(0,10);
    // console.log(clase);
    //
    //
    // let clase2 = $(el).attr('class');
    // let clase3 = clase2.substr(0,10);

    // if( clase3 === clase1 ){
    //     drake.cancel(true);
    //
    //     swal({
    //         title: "Sorry!",
    //         text: "No puedes mover la primer condición",
    //         type: "error"
    //     });
    // }

}).on('drop', function(el, target, source, sibling){
    // alert('Entre Drop');

    let pimerCondiTr = $('#drag-elements').children().eq(0);

    if ( pimerCondiTr.hasClass('parCierre') ){
        drake.cancel(true);

        swal({
            title: "Sorry!",
            text: "Tu condicion no puede iniciar un Parentesis de cierre",
            type: "error"
        });
    } else {

        let indexHijo = $(el).index();

        let primerCondi = $('#drag-elements').children();
        // console.log(primerCondi);
        //
        // console.log(primerCondi);
        // let clase = primerCondi.attr('class');
        // let clase1 = clase.substr(0,10);
        // console.log(clase1);
        //
        // console.log(el);
        // let clase2 = $(el).attr('class');
        // let clase3 = clase2.substr(0,10);
        // console.log(clase3);

        if( indexHijo === 0){
            alert('Son iguales');

            let td1 = $(el).children().eq(1);
            let td3 = $(el).children().eq(3);
            let td4 = $(el).children().eq(4);

            td1.html('');
            td3.html('');
            td4.html('-1');

            let tr2 = primerCondi.eq(1);
            // console.log(tr2);

            tr2.children().eq(1).html('<span class="btn btn-primary btn-xs changeConector">AND</span>');
            tr2.children().eq(3).html('<span type="button" class="btn btn-danger btn-xs eliminar"><i class="fa fa-times" aria-hidden="true"></i></span>');
            tr2.children().eq(4).html('1');

        }
    }

});

//######## AGREGAR CONDICIONES Y VALIDACIONES PARA NO REPETIR CONDICIONES #######################
let arrayOneValue = [];

let arrayBetween = [];

let arrayList = [];

//---Número de Clase
let numCon = 0;

//---Efecto fade
let fadeIn = 1000;

$('.addConditions').on('click', function(){

    let conditionText;
    let denegadoBetweenList;

    //-- Texto de los Select
    let campoText = $('.selectFeild option:selected').text();
    let operadorText = $('.operator_sel option:selected').text();
    let listText = $('#selectList option:selected').text();
    let listUpload = $('#customText').text();
    let connector =  $('#selectConnector option:selected').text();

    //-- Valores de los Select
    let campoVal = $('.selectFeild').val();
    let operadorVal = $('.operator_sel').val();
    let conectorVal = $('#selectConnector').val();

    //-- Valores de los Inputs
    let unValor = $('#valor').val();
    let valOneBetween = $('.valOneBetween').val();
    let valTwoBetween = $('.valTwoBetween').val();

    //-- Check de Negado
    let denied = $('#denied').val();

    //---Agrega la palabra "NO" - menos Between / Not Between - In / Not In
    if( denied === "1"){

        negado = "No";
        tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;

    } else if ( denied === "0" ) {

        negado = "";
        tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;

    }

    //---GON TABLAS
    let campos = gon.tCamposLays;

    //-- Guardo la posision y longitud del campo selecionado en una td
    for( let i = 0; i < campos.length; i++ ){

        let campo = campos[i];

        if( campo.IdCampo == campoVal ){

            // alert(`El campo: ${campoVal} coincide con campo: ${campo.IdCampo}`);

            td = `<td class="tdDispNone filterCondi" value="${campo.Posicion}" name="posision">${campo.Posicion}</td>
                          <td class="tdDispNone filterCondi" value="${campo.Longitud}" name="longitud">${campo.Longitud}</td>`;
        }

    }


    let numDeCondi = $('.tBodyCondition').children().length;
    if( numDeCondi === 0 ){
        conectorVal = -1;
        // connector = "";
        tdConector = `<td class="text-center connector"> </td>`;
        tdEliminar = `<td class="text-center"> </td>`;
    } else {
        tdConector = `<td class="text-center connector"> <span class="btn btn-primary btn-xs changeConector" title="Cambiar conector">${connector}</span></td>`;
        tdEliminar = `<td class="text-center"><span class="btn btn-danger btn-xs eliminar" type="button" title="Delete"><i class="fa fa-times"></i></span></td>`;
    }

    if ( "" !== unValor) {

        numCon++;

        //---Creo una variable donde concateno todos los valores
        let conditionText = `${campoText} ${operadorText} ${unValor}`;

        //value="${conditionText}" name="${numCon}" title="${operadorText}"
        let tr = `<tr class="condicion${numCon} condicion" value="${conditionText}" name="${operadorText}"> 
                      ${tdDenegado}
                      ${tdConector}
                      <td class="text-center expresion"><strong class="negado">${negado}</strong> <span class="campo" value="${campoVal}">${campoText}</span> <strong class="operador" value="${operadorVal}">${operadorText}</strong> <span class="valorUno" value="${unValor}">${unValor}</span> </td>
                      ${tdEliminar}
                      <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
                      <td class="tdDispNone filterCondi denegadoVal" value="${denied}" name="denegado">${denied}</td>
                      <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
                      <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
                      <td class="tdDispNone filterCondi" value="${unValor}" name="valor">${unValor}</td>
                      <td class="tdDispNone filterCondi" value="${campoVal}" name="campo">${campoVal}</td>
                      ${td}
                  </tr>`;


        if ( arrayOneValue.length !== 0 ){

            let isDuplicateOne = false;

            //---Recorro todos los valores almacenados en el arreglo para checar si ya existe la condición
            for ( let i = 0; i < arrayOneValue.length; i++ ){

                //---- Esta variable itera todos los valores del arreglo
                let conditionArr = arrayOneValue[i];

                //---Camparo valores obtenidos de los inputs con los valores que ya existen en el arreglo
                if ( conditionText === conditionArr ){
                    numCon--;

                    swal({
                        title: "Sorry!",
                        text: "Esta Condicion Ya Existe",
                        type: "error"
                    });

                    isDuplicateOne = true;

                }
            }

            //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
            if ( !isDuplicateOne ){

                let appCondition = $('.tBodyCondition').append($(tr).fadeIn(fadeIn));

                $('.s_Condition').scrollTop( appCondition.offset().top );

                arrayOneValue.push(`${conditionText}`);
            }

            console.log(arrayOneValue);

        } else{

            arrayOneValue.push(`${conditionText}`);

            console.log(arrayOneValue);

            let appCondition = $('.tBodyCondition').append($(tr).fadeIn(fadeIn));

            $('.s_Condition').scrollTop( appCondition.offset().top );


        }

        //---Se limpian valores
        $('.selectFeild').val("");
        $('.operator_sel').val("");
        $('#selectConnector').val("");
        $('#valor').val("");
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

        ///-----Valido que los inputs de Between no esten vacios
    }
    else if ( valOneBetween !== "" && valTwoBetween !== "" ){

        numCon++;

        //---Creo Una variable donde concateno los valores Obtenidos de los inputs Between
        conditionText = `${campoText} Between ${valOneBetween} AND ${valTwoBetween}`;

        if( operadorText === " Between "){

            denegadoBetweenList = 0;
            negadoBetIn = "";
            tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;

        } else {

            denegadoBetweenList = 1;
            negadoBetIn = "No";
            tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;
        }

        //---Varibales para construir la Fila
        // let parAbre = `<strong class="parAbre" value="1">${parentesis}</strong>`;
        // let parCierre = `<strong class="parCierra" value="2">)</strong>`;
        let negadoCampo = `<strong class="negado">${negadoBetIn}</strong>`;
        let campo = `<span class="campo" value="${campoVal}">${campoText}</span>`;
        let operador = `<strong class="operador">Between</strong>`;
        // let menorQue = '<strong class="operador"><=</strong>';
        let valorUno = `<span class="valorUno" value="${valOneBetween}">${valOneBetween}</span>`;
        let valorDos = `<span class="valorDos" value="${valTwoBetween}">${valTwoBetween}</span>`;
        let conectorAnd = '<strong class="conector">AND</strong>';

        let tr = `<tr class="condicion${numCon} condicion" value="${conditionText}" name="${operadorText}">
                     ${tdDenegado}
                     ${tdConector}
                     <td class="text-center expresion">${negadoCampo} ${campo} ${operador} ${valorUno} ${conectorAnd} ${valorDos}</td>
                     ${tdEliminar}
                     <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
                     <td class="tdDispNone filterCondi denegadoVal" value="${denegadoBetweenList}" name="denegado">${denegadoBetweenList}</td>
                     <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
                     <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
                     <td class="tdDispNone filterCondi" value="{${valOneBetween}}{${valTwoBetween}}" name="valor">{${valOneBetween}}{${valTwoBetween}}</td>
                     <td class="tdDispNone filterCondi" value="${campoVal}" name="campo">${campoVal}</td>
                     ${td}
                  </tr>
                  `;

        //---Si hay elementos en el arreglo se ejecuta las sig. lineas
        if ( arrayBetween !== 0){

            let isDuplicateTwo = false;

            //---Recorro todos los valores almacenados en el arreglo Between
            for ( let i = 0; i < arrayBetween.length; i++ ){

                //---- Esta variable itera todos los valores del arreglo
                let conditionArr = arrayBetween[i];
                console.log(conditionArr);

                //---Si los valores obetenidos de Feild, Operatos e Inputs Between ya existen en el Arreglo se ejecuta las sig. lineas
                if ( conditionText == conditionArr ){
                    numCon--;

                    swal({
                        title: "Sorry!",
                        text: "Esta Condicion Ya Existe",
                        type: "error"
                    });

                    isDuplicateTwo = true;

                }
            }

            //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
            if ( !isDuplicateTwo ){

                //---Imprime Fila
                $('.tBodyCondition').append($(tr).fadeIn(fadeIn));


                // for ( let i = 0; i < conditionText.length; i++ ){
                //     let condicion = conditionText[i];
                //
                //     let filaBetween = `<tr class="condicion${numCon}">
                //                           <td class="text-center"><strong class="conditionCreate">${condicion}</strong></td>
                //                           <td class="text-center"></td>
                //                        </tr>`;
                //
                //     tr = $('.tBodyCondition').append($(filaBetween).fadeIn(fadeIn));
                //
                //     $('.s_Condition').scrollTop( tr.offset().top );
                // }

                arrayBetween.push( `${conditionText}` );
            }

            console.log(arrayBetween);

        }
        else{

            //---Imprime Fila
            $('.tBodyCondition').append($(tr).fadeIn(fadeIn));


            // for ( let i = 0; i < conditionText.length; i++ ){
            //     var condicion = conditionText[i];
            //
            //     let filaBetween = `<tr class="condicion${numCon}">
            //                             <td class="text-center"><strong class="conditionCreate">${condicion}</strong></td>
            //                             <td class="text-center"></td>
            //                          </tr>`;
            //
            //     tr = $('.tBodyCondition').append($(filaBetween).fadeIn(fadeIn));
            //
            //     $('.s_Condition').scrollTop( tr.offset().top );
            // }

            arrayBetween.push(`${conditionText}`);
            console.log(arrayBetween);
        }



        //---Se limpian los valores
        $('.selectFeild').val("");
        $('.operator_sel').val("");
        $('#selectConnector').val("");
        $('.valOneBetween').val("");
        $('.valTwoBetween').val("");
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');
    }
    else if ( listText !== "Select List" || listUpload !== "" ) {

        numCon++;

        //---Creo una variable donde concateno todos los valores
        conditionText = `${campoText} In ${listUpload}`;

        if ( listText !== "Select List" ){

            var lista = `${listText}`,
                listVal = $('#selectList').val();

        } else {

            var lista = `${listUpload}`,
                listVal = `${listUpload}`;
        }

        if ( operadorText === " In "){

            denegadoBetweenList = 0;
            negadoBetIn = "";
            tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;

        } else {

            denegadoBetweenList = 1;
            negadoBetIn = "No";
            tdDenegado = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;

        }

        let tr = `<tr class="condicion${numCon} condicion" value="${conditionText}" name="${operadorText}">
                      ${tdDenegado}
                      ${tdConector}
                      <td class="text-center expresion"><strong class="negado">${negadoBetIn}</strong> <span class="campo" value="${campoVal}">${campoText}</span> <strong class="operador" value="${operadorVal}">In</strong> <span class="lista" value="${listVal}"> ${lista} </span> </td>
                      ${tdEliminar}
                      <td class="tdDispNone filterCondi conectorVal" value="${conectorVal}" name="conector">${conectorVal}</td>
                      <td class="tdDispNone filterCondi denegadoVal" value="${denegadoBetweenList}" name="denegado">${denegadoBetweenList}</td>
                      <td class="tdDispNone filterCondi" value="${operadorVal}" name="operador">${operadorVal}</td>
                      <td class="tdDispNone filterCondi" value="0" name="parantesis">0</td>
                      <td class="tdDispNone filterCondi" value="${lista}" name="valor">${lista}</td>
                      <td class="tdDispNone filterCondi" value="${campoVal}" name="campo">${campoVal}</td>
                      ${td}
                    </tr>`;


        if ( arrayList.length !== 0 ){

            var isDuplicateOne = false;

            //---Recorro todos los valores almacenados en el arreglo para checar si ya existe la condición
            for ( let i = 0; i < arrayList.length; i++ ){

                //---- Esta variable itera todos los valores del arreglo
                var conditionArr = arrayList[i];
                console.log(conditionArr);

                //---Camparo valores obtenidos de los inputs con los valores que ya existen en el arreglo
                if ( conditionText === conditionArr ){
                    numCon--;

                    swal({
                        title: "Sorry!",
                        text: "Esta Condicion Ya Existe",
                        type: "error"
                    });

                    isDuplicateOne = true;

                }
            }

            //---Si isDuplicate es diferente de "false" se ejecuta el código de adentro
            if ( !isDuplicateOne ){


                var appCondition = $('.tBodyCondition').append($(tr).fadeIn(fadeIn));

                $('.s_Condition').scrollTop( appCondition.offset().top );

                arrayList.push(`${conditionText}`);
            }

            console.log(arrayList);


        } else{

            let appCondition = $('.tBodyCondition').append($(tr).fadeIn(fadeIn));

            $('.s_Condition').scrollTop( appCondition.offset().top );

            arrayList.push(`${conditionText}`);

            console.log(arrayList);
        }

        $('.selectFeild').val("");
        $('.operator_sel').val("");
        $('#selectConnector').val("");
        $('#selectList').val("");
        $('#realBtnFile').val("");
        $('.customText').html('No se eligió archivo')
        $('#denied').val('0');
        $('.icheckbox_square-green').removeClass('checked');

    }


    //--Habilita select de los conectores
    $('#selectConnector').prop('disabled', false);
});


function eliminarCondicion(elemento, arrayConditions ){
    // alert("Entre Funcion borrar arreglo");
    // console.log(elemento);
    // console.log(arrayBetween);

    for ( let k in arrayConditions ){
        let condicion = arrayConditions[k];

        if ( elemento === condicion){
            // alert(` Estos ${elemento} es igual a esto ${condicion}`);

            let indice = arrayConditions.indexOf(condicion);

            arrayConditions.splice(indice, 1);

            console.log(arrayConditions);

            swal({
                title: "OK!",
                text: "Condicion Eliminada",
                type: "success"
            });

        }
    }
}

//-- Agregar bloque de Parenthesis
$('#selectParentesis').on('change', function(){
   // alert("Entre Parentesis");

   let longCondi = $('.tBodyCondition').children().length;
   let valorConector = $(this).val();
   let parText = $('#selectParentesis option:selected').text();
   let parConector = $('#selectParentesis option:selected').attr('name');


    //---Agrega la palabra "NO" - menos Between / Not Between - In / Not In
    if( parText === "NOT AND(" || parText === "NOT OR(" || parText === "NOT(" ){

        negadoPar = "No";
        deniedPar = 1;
        tdDenegadoPar = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied" checked></td>`;

    } else {

        negadoPar = "";
        deniedPar = 0;
        tdDenegadoPar = `<td class="text-center denegado"><input type="checkbox" title="Cambiar denegado" class="changeDenied"></td>`;

    }


    numCon++;

    let abrePartr = `<tr class="condicion${numCon} condicion parAbre">
                  ${tdDenegadoPar}
                  <td class="text-center connector"> <span class="btn btn-primary btn-xs changeConector" title="Cambiar conector">${parConector}</span></td>
                  <td class="text-center expresion"><strong>${numCon} - </strong> <strong class="negado">${negadoPar}</strong> <span class="parenthesis">(</span></td>
                  <td class="text-center"><span class="btn btn-danger btn-xs eliminar" type="button" title="Delete"><i class="fa fa-times"></i></span></td>
                  <td class="tdDispNone filterCondi conectorVal" value="${valorConector}" name="conector">${valorConector}</td>
                  <td class="tdDispNone filterCondi denegadoVal" value="${deniedPar}" name="denegado">${deniedPar}</td>
                  <td class="tdDispNone filterCondi" value="0" name="operador">0</td>
                  <td class="tdDispNone filterCondi" value="1" name="parantesis">1</td>
                  <td class="tdDispNone filterCondi" value="0" name="valor">0</td>
                  <td class="tdDispNone filterCondi" value="0" name="campo">0</td>
                  <td class="tdDispNone filterCondi" value="0" name="posision">0</td>
                  <td class="tdDispNone filterCondi" value="0" name="longitud">0</td>
              </tr>`;


    let cierrePartr = `<tr class="condicion${numCon} condicion parCierre">
                              <td class="text-center enegado"></td>
                              <td class="text-center connector"></td>
                              <td class="text-center expresion parCierre"><strong>${numCon} - </strong> <strong class="parenthesis">)</strong></td>
                              <td class="text-center"></td>
                              <td class="tdDispNone filterCondi conectorVal" value="-1" name="conector">-1</td>
                              <td class="tdDispNone filterCondi denegadoVal" value="0" name="denegado">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="operador">0</td>
                              <td class="tdDispNone filterCondi" value="2" name="parantesis">2</td>
                              <td class="tdDispNone filterCondi" value="0" name="valor">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="campo">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="posision">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="longitud">0</td>
                          </tr>`;

   if( longCondi < 1) {
       // alert("Entre mayor");

       if( parText === "AND(" || parText === "OR(" || parText === "NOT AND(" || parText === "NOT OR(" ){
           swal({
               title: "ERROR!",
               text: "No puedes agregar un bloque inicial con Conector",
               type: "error"
           });

           numCon--;

       } else {

           abrePartr = `<tr class="condicion${numCon} condicion parAbre"> 
                              ${tdDenegadoPar}
                              <td class="text-center connector"></td>
                              <td class="text-center expresion"> <strong>${numCon} - </strong> <strong class="negado">${negadoPar}</strong> <strong class="parenthesis">(</strong></td>
                              <td class="text-center"></td>
                              <td class="tdDispNone filterCondi conectorVal" value="-1" name="conector">-1</td>
                              <td class="tdDispNone filterCondi denegadoVal" value="${deniedPar}" name="denegado">${deniedPar}</td>
                              <td class="tdDispNone filterCondi" value="0" name="operador">0</td>
                              <td class="tdDispNone filterCondi" value="1" name="parantesis">1</td>
                              <td class="tdDispNone filterCondi" value="0" name="valor">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="campo">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="posision">0</td>
                              <td class="tdDispNone filterCondi" value="0" name="longitud">0</td>
                          </tr>`;

           $('.tBodyCondition').append($(abrePartr).fadeIn(fadeIn) );
           let appCondition = $('.tBodyCondition').append($(cierrePartr).fadeIn(fadeIn) );

           $('.s_Condition').scrollTop( appCondition.offset().top );

           //--Habilita select de los conectores
           $('#selectConnector').prop('disabled', false);

       }

   } else {

       $('.tBodyCondition').append($(abrePartr).fadeIn(fadeIn) );
       let appCondition = $('.tBodyCondition').append($(cierrePartr).fadeIn(fadeIn) );

       $('.s_Condition').scrollTop( appCondition.offset().top );
   }

   $(this).val("");

});


//--Cambiar Negado en la condicion
$('.tBodyCondition').on('click', 'input.changeDenied', function(){
    // alert("entre Conector");

    let tdPadre = $(this).parent();
    let tdDenegado = tdPadre.siblings('td.denegadoVal');
    let strongNo = tdPadre.siblings('td.expresion').find('strong.negado');
    // let strongNo = tdExpresion.children();

    if ( tdDenegado.html() === "0" ){
        tdDenegado.html('1');
        strongNo.html('No');
    } else {
        tdDenegado.html('0');
        strongNo.html('');
    }

});


//--Cambiar conector
$('.tBodyCondition').on('click', 'span.changeConector', function(){
    // alert("entre Conector");

    let tdPadre = $(this).parent();
    let tdConector = tdPadre.siblings('td.conectorVal');

    if ( tdConector.html() === "0" ){

        tdConector.html('1');
        $(this).html('AND');

    } else {
        tdConector.html('0');
        $(this).html('OR');
    }

});

//----Eliminar elementos del Filtro
//----El evento click esta siendo detectado por el contenedor padre, pero este asu vez delega el evento a sus hijos en este caso di.clone
$('.tBodyCondition').on('click', 'span.eliminar', function() {
    // alert("Entre eliminar");

    let trPadre = $(this).parents('tr');
    let esPar = trPadre.hasClass('parAbre');
    let clase = trPadre.attr('class');
    let claseNum = clase.substr(9,1); //-- Obtengo en numero de la condicion
    // console.log(claseNum);

    let value = trPadre.attr('value');
    let name = trPadre.attr('name');

    swal({
            title: "Eliminar!",
            text: "Estas seguro de eliminar esta condición",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Confirmar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {

            if (isConfirm){

                swal({
                        title: "Eliminar!",
                        text: "Confirme para eliminar la Condicion",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Confirmar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {

                        if (isConfirm){


                            if( !esPar ){
                                $(`tr.condicion${claseNum}`).remove();

                                if ( name === " Between " || name === " Not Between " ){

                                    eliminarCondicion(value, arrayBetween);

                                } else if ( name === " In " || name === " Not In " ){

                                    eliminarCondicion(value, arrayList);

                                } else {

                                    eliminarCondicion(value, arrayOneValue);

                                }
                            } else {

                                $(`tr.condicion${claseNum}`).remove();

                                swal({
                                    title: "OK!",
                                    text: "Condicion Eliminada",
                                    type: "success"
                                });
                            }

                        }
                        else{
                            swal("Cancelado!","Eliminacion cancelada", "error");
                        }
                    });


            } else {
                swal("Cancelado!","Eliminacion cancelada", "error");
            }

        });

});


arrayFilter = new Array();
// //--### AGREGAR CONECTORES Y VALIDACIONES ###---
// $('#selectConnector').on('change', function(){
//     // alert("Fuck!!!")
//     let addConector = $('.drop-target').children().length
//
//     if ( addConector !== 1 ) {
//
//         let conectorVal = $('#selectConnector').val();
//         let connector =  $('#selectConnector option:selected').text();
//         // alert(selects)
//
//         if ( (connector === " AND( ") || (connector === " OR( ") ||  (connector === " ( ")) {
//             $('.drop-target').append(`<tr class="conectorP">
//                                           <td colspan="3"> ${connector} </td>
//                                           <td class="tdDispNone filterCondi" name="conector">${conectorVal}</td>
//                                           <td class="tdDispNone filterCondi" name="denegado">0</td>
//                                           <td class="tdDispNone filterCondi" name="operador">0</td>
//                                           <td class="tdDispNone filterCondi" name="parentesis">1</td>
//                                           <td class="tdDispNone filterCondi" name="value">0</td>
//                                           <td class="tdDispNone filterCondi" name="campo">0</td>
//                                           <td class="tdDispNone filterCondi" name="posision">0</td>
//                                           <td class="tdDispNone filterCondi" name="longitud">0</td>
//                                       </tr>
//                                       <tr class="trPar"> <td colspan="3"> Arrastra aquí otra condición </td> </tr>
//                                       <tr class="conectorP">
//                                           <td colspan="3"> ) </td>
//                                           <td class="tdDispNone filterCondi" name="conector">-1</td>
//                                           <td class="tdDispNone filterCondi" name="denegado">0</td>
//                                           <td class="tdDispNone filterCondi" name="operador">0</td>
//                                           <td class="tdDispNone filterCondi" name="parentesis">2</td>
//                                           <td class="tdDispNone filterCondi" name="value">0</td>
//                                           <td class="tdDispNone filterCondi" name="campo">0</td>
//                                           <td class="tdDispNone filterCondi" name="posision">0</td>
//                                           <td class="tdDispNone filterCondi" name="longitud">0</td>
//                                       </tr>
//                                        `);
//
//         } else {
//             $('.drop-target').append(`<tr class="connector">
//                                           <td colspan="3">${connector}</td>
//                                           <td class="tdDispNone filterCondi">${conectorVal}</td>
//                                         </tr>
//                                       `);
//         }
//
//         $(this).val("");
//
//     } else {
//
//         $(this).val("");
//
//         swal({
//             title: "Sorry!",
//             text: "Debes de arrastar una candicion dentro del bloque primero para ocupar un conector",
//             type: "error"
//         });
//
//     }
// });

//----Guardar Filtro
$('.saveFilter').on('click', function(){
    // alert("Entre guardar");
    // var filtro = $('.container').text();
    // var filtroHijos = $('.container').children().length;
    // var nameFilter = $('.nameFilter').val();
    // var aliasFilter = $('.aliasFilter').val();

    let valor = "";
    let td = $('.tBodyCondition').find('td.filterCondi');
    let cont = 0;
    let numCon = 0;
    let addCondicion = false;

    td.each(function(){
        valor = $(this).html();
        cont++;
        console.log(cont);

        if( cont === 1){
            console.log(`Conector: ${valor}`);
            conectorVal = parseInt(valor);
        } else if( cont === 2){
            console.log(`Negado: ${valor}`);
            negadoVal = parseInt(valor);
        } else if( cont === 3){
            console.log(`Operador: ${valor}`);
            opeVal = parseInt(valor);;
        } else if( cont === 4){
            console.log(`Parentesis: ${valor}`);
            parVal = parseInt(valor);;
        } else if( cont === 5){
            console.log(`Valor: ${valor}`);
            valorVal = valor;
        } else if( cont === 6){
            console.log(`Id Campo: ${valor}`);
            idCampo = parseInt(valor);
        } else if( cont === 7){
            console.log(`Posision: ${valor}`);
            posVal = parseInt(valor);
        } else if( cont === 8){
            console.log(`Longitud: ${valor}`);
            longVal = parseInt(valor);
            cont = 0;

            addCondicion = true;
        }

        if ( addCondicion === true ){
            alert("Entre Ajax");
            addCondicion = false;
            numCon++;
            $.ajax({
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                },
                url: "/tx_filter_details",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "_method": "create",
                    filter_id: 5,
                    conditionnumber: numCon,
                    connector: conectorVal,
                    denied: negadoVal,
                    operator: opeVal,
                    parenthesis: parVal,
                    value: valorVal,
                    fieldFormat_id: idCampo,
                    position: posVal,
                    lenght: longVal
                    // keyId: keyId.value,
                    // value: valor.value,
                    //condition: contador,
                }),
                success: function () {
                    // cargar();
                    // valor.value = "";
                    // operator.value = "";
                    // connector.value = "";
                    // parenthesis.value = "";
                    // longitud.value = "";
                    // position.value = "";
                    // denied.value = "";
                    // $("#oper1").removeClass("active");
                    // $("#oper2").removeClass("active");
                    // $("#oper3").removeClass("active");
                    // $("#oper4").removeClass("active");
                    // $("#oper5").removeClass("active");
                    // $("#oper6").removeClass("active");
                    // $("#oper7").removeClass("active");
                    // $("#par1").removeClass("active");
                    // $("#par2").removeClass("active");
                    // $("#con2").removeClass("active");
                    // $("#con1").removeClass("active");
                    // $('#flay').val("");

                    swal({
                        title: "Good job!",
                        text: "Condition added!",
                        type: "success"
                    });

                }
            });
        }

    });


    // if ( filtro != "" || filtroHijos != 0){
    //
    //     if ( nameFilter != "" && aliasFilter != "" ){
    //
    //         $('.contFilters').append(`<div class="btn btn-primary btn-width" title="${aliasFilter}"> <span>${nameFilter}</span> </div>`)
    //         $('.nameFilter').val("");
    //         $('.aliasFilter').val("");
    //
    //         let camposConidciones = $('.drop-target').find('td.filterCondi');
    //
    //         camposConidciones.each(function() {
    //             console.log(camposConidciones)
    //         });
    //
    //     } else {
    //
    //         swal({
    //             title: "Sorry!",
    //             text: "No dejes campos vacíos, para guardar el filtro",
    //             type: "error"
    //         });
    //
    //     }
    //
    // } else {
    //
    //     swal({
    //         title: "Sorry!",
    //         text: "No puedes guardar un filtro vacío",
    //         type: "error"
    //     });
    // }

});


