class CreateTableOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :table_orders do |t|
      t.integer :user_id
      t.string :name_column
      t.integer :order
      t.string :table_id

      t.timestamps
    end
  end
end
