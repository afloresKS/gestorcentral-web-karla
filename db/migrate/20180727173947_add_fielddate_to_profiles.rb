class AddFielddateToProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :day, :integer
    add_column :profiles, :month, :integer
  end
end
