class AddEndclockToGestorSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :gestor_settings, :blue_day_end, :string
    add_column :gestor_settings, :blue_night_end, :string
    add_column :gestor_settings, :yellow_day_end, :string
    add_column :gestor_settings, :yellow_night_end, :string
    add_column :gestor_settings, :red_day_end, :string
    add_column :gestor_settings, :red_night_end, :string
  end
end
