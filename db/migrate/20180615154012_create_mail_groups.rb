class CreateMailGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :mail_groups do |t|
      t.integer :group_id
      t.string :correo
      t.timestamps
    end
  end
end
