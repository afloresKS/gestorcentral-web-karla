require 'test_helper'

class CategoriFiltersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @categori_filter = categori_filters(:one)
  end

  test "should get index" do
    get categori_filters_url
    assert_response :success
  end

  test "should get new" do
    get new_categori_filter_url
    assert_response :success
  end

  test "should create categori_filter" do
    assert_difference('CategoriFilter.count') do
      post categori_filters_url, params: { categori_filter: { id_categori: @categori_filter.id_categori, id_filter: @categori_filter.id_filter } }
    end

    assert_redirected_to categori_filter_url(CategoriFilter.last)
  end

  test "should show categori_filter" do
    get categori_filter_url(@categori_filter)
    assert_response :success
  end

  test "should get edit" do
    get edit_categori_filter_url(@categori_filter)
    assert_response :success
  end

  test "should update categori_filter" do
    patch categori_filter_url(@categori_filter), params: { categori_filter: { id_categori: @categori_filter.id_categori, id_filter: @categori_filter.id_filter } }
    assert_redirected_to categori_filter_url(@categori_filter)
  end

  test "should destroy categori_filter" do
    assert_difference('CategoriFilter.count', -1) do
      delete categori_filter_url(@categori_filter)
    end

    assert_redirected_to categori_filters_url
  end
end
