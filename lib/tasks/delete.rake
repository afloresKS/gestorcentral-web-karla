namespace :delete do
  task xls_files: :environment do
    puts cyan('>------------------------------------------------------------------------<')
    puts cyan('XLS FILES REMOVAL PROCESS STARTS...')
    puts cyan(Time.now.to_s)

    output_alerts = `find public/generatedXLS/alerts/* -mtime +30 -type f `
    output_con_gen = `find public/generatedXLS/con_gen/* -mtime +30 -type f `
    output_niv_alert = `find public/generatedXLS/niv_alert/* -mtime +30 -type f `

    xls_alert = output_alerts.split("\n")
    xls_con_gen = output_con_gen.split("\n")
    xls_niv_alert = output_niv_alert.split("\n")

    total_xls_files = xls_alert.length + xls_con_gen.length + xls_niv_alert.length

    puts 'Files to delete: ' + total_xls_files.to_s

    if total_xls_files > 0
      delete = `find public/generatedXLS/alerts/* -mtime +30 -type f -delete`
      delete = `find public/generatedXLS/con_gen/* -mtime +30 -type f -delete`
      delete = `find public/generatedXLS/niv_alert/* -mtime +30 -type f -delete`

      puts 'Files deleted'

      output_alerts = `find public/generatedXLS/alerts/* -mtime +30 -type f `
      output_con_gen = `find public/generatedXLS/con_gen/* -mtime +30 -type f `
      output_niv_alert = `find public/generatedXLS/niv_alert/* -mtime +30 -type f `

      xls_alert = output_alerts.split("\n")
      xls_con_gen = output_con_gen.split("\n")
      xls_niv_alert = output_niv_alert.split("\n")

      total_xls_files = xls_alert.length + xls_con_gen.length + xls_niv_alert.length

      if total_xls_files > 0
        puts 'Files missing to delete: ' + total_xls_files.to_s
      end

    else
      puts red('No files to delete')
    end
    puts green('Finished at: ' + Time.now.to_s)

  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def magenta(mytext)
    ; "\e[35m#{mytext}\e[0m"
  end
end